#include "kdTree.cpp"

using namespace std;

int main() {
    vector<KDTree*> clasificador;
    vector<string> dataTraining;
    vector<vector<BPQ>> matches;
    vector<int> matchesPerImages;
    string cmdline;
    vector<vector<int>> tempVector;
    dataTraining = getFolderFiles("./resources/training-data");

    //creando kd-tree
    for(int i=0;i<dataTraining.size();i++){
        cmdline =  "python3 main.py ./resources/training-data/"+dataTraining[i];
        system(cmdline.c_str());
        tempVector = parseVector("vector.txt");
        clasificador.push_back(new KDTree(tempVector));
    }
    
    matches.resize(clasificador.size());
    matchesPerImages.resize(clasificador.size());
    cmdline = "python3 main.py ./resources/querys/test.jpg";
    system(cmdline.c_str());
    tempVector = parseVector("vector.txt");

    for(int i=0;i<clasificador.size();i++){
        for(int j=0;j<tempVector.size();j++){
            matches[i].push_back(clasificador[i]->KNNClassifier(tempVector[j],2));
        }
    }

    for (int i=0;i< matches.size(); i++){
        for(int j=0;j<matches[i].size();j++){
            if (matches[i][j].queue[0].second < 0.7 * matches[i][j].queue[1].second){
                matchesPerImages[i]++;
            }
        }
    }

    for(int i=0;i<matchesPerImages.size();i++){
        cout<<matchesPerImages[i]<<" ";
    }
    cout<<endl;
}