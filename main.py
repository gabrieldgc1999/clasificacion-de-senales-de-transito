import numpy as np
import cv2
import sys
from matplotlib import pyplot as plt
imgname = sys.argv[1]
print(imgname)

## Create SIFT object
sift = cv2.SIFT_create()

## Detect and compute
img1 = cv2.imread(imgname)
gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
kpts1, descs1 = sift.detectAndCompute(gray1,None)

## Creando ficheros
f =  open('vector.txt','w')
cadena = ""
for row in descs1:
    for elem in row:
        cadena += str(elem) + " "
    cadena += "\n"
f.write(cadena)
f.close()