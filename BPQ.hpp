#ifndef BPQ_H
#define BPQ_H

#include <iostream>
#include <vector>
#include <algorithm>

bool sortDistance(std::pair<std::vector<int>, double> point1, std::pair<std::vector<int>, double> point2) {
    return point1.second < point2.second;
}

class BPQ {
    public:
        BPQ(int size);
        void insert(std::vector<int> point, double priority);
        void printPoints();
        int size;
        std::vector<std::pair<std::vector<int>, double>> queue;
};

BPQ::BPQ(int size) {
    this->size = size;
}

void BPQ::insert(std::vector<int> point, double priority) {
    
    if(this->queue.size() < this->size) {
        std::pair<std::vector<int>, double> temp(point, priority);
        this->queue.push_back(temp);

    } else {
        if(this->queue[this->queue.size() - 1].second > priority) {
            this->queue[this->queue.size() - 1].first = point;
            this->queue[this->queue.size() - 1].second = priority;
        }
    }
    std::sort(queue.begin(), queue.end(), sortDistance);
}

void BPQ::printPoints() {
    for(int i = 0; i < queue.size(); ++i) {
        for(int j = 0; j < queue[i].first.size(); ++j)
            std::cout << queue[i].first[j] << " ";
        std::cout << std::endl;
    }
}

#endif