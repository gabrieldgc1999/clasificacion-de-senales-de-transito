#include <vector>
#include <math.h>
#include <iostream>
#include <fstream>

#define ESPACIO 32

void merge(std::vector<std::vector<int>>& points, int left, int mid, int right, int dim) {
    std::vector<std::vector<int>> temp;
    int i = left;
    int j = mid + 1;

    while(i <= mid && j <= right) {
        if(points[i][dim] <= points[j][dim])
            temp.push_back(points[i++]);
        else
            temp.push_back(points[j++]);
    }

    while(i <= mid)
        temp.push_back(points[i++]);
    while(j <= right)
        temp.push_back(points[j++]);

    for(int i = left, j = 0; i <= right; ++i, ++j)
        points[i] = temp[j];
}

void mergeSort(std::vector<std::vector<int>>& points, int left, int right, int dim) {
    int mid = floor((left + right) / 2);

    if(left < right) {
        mergeSort(points, left, mid, dim);
        mergeSort(points, mid + 1, right, dim);
        merge(points, left, mid, right, dim);
    }   
}

void printCoordinates(std::vector<std::vector<int>> points) {
    for(int i = 0; i < points.size(); ++i) {
        for(int j = 0; j < points[i].size(); ++j)
            std::cout << points[i][j] << " ";
        std::cout << std::endl;
    }
}

double distanceSquared(std::vector<int> point1, std::vector<int> point2) {
    double distance = 0;
    for(int i = 0; i < point1.size(); ++i)
        distance += std::pow(point1[i] - point2[i], 2);
    return std::sqrt(distance);
}

void closestPointBruteForce(std::vector<std::vector<int>> points, std::vector<int> point) {
    std::vector<int> closestPoint;
    double minDistance, distance;
    for(int i = 0; i < points.size(); ++i) {
        if(i == 0){
            minDistance = distanceSquared(points[0], point);
            closestPoint = points[0];
        }
       
        distance = distanceSquared(points[i], point); 
        if(minDistance >= distance) {
            minDistance = distance;
            closestPoint = points[i];
        }
    }

    for(int i = 0; i < closestPoint.size(); ++i)
        std::cout << closestPoint[i] << " ";
    std::cout << std::endl;
}

std::vector<std::vector<int>> parseVector(std::string file){
    std::vector<std::vector<int>> matrix;
    std::vector<int> row;
    std::ifstream vectorFile;
    vectorFile.open(file, std::ios::in | std::ios::binary);
    double number;
    if(vectorFile.is_open()){
        std::string line;
        while(std::getline(vectorFile, line)){
            int i = 0;
            std::string numberInString;
            while(line[i] != '\0'){

                if(line[i] == ESPACIO){
	                row.push_back(std::stoi(numberInString));                    
                    numberInString = "";
                }
                else
                    numberInString += line[i];
				i++;
            }
            line = "";
            matrix.push_back(row);           
			row.clear();
        }
        vectorFile.close();
    }
	return matrix;
}

std::vector<std::string> getFolderFiles(std::string folderPath){
    std::string bashCommand = "ls " + folderPath + " > folderFiles";
    system(bashCommand.c_str());
    std::vector<std::string> files;
    std::ifstream folderFiles;
    folderFiles.open("folderFiles", std::ios::in | std::ios::binary);
    std::string file;
    if(folderFiles.is_open()){
        while(folderFiles >> file){
            files.push_back(file);
        }
    }
    folderFiles.close();
    return files;
}
