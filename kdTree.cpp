#include "kdTree.hpp"

KDTree::KDTree() {
    this -> root = nullptr;    
}

KDTree::~KDTree() {
    empty(this->root);
}

void KDTree::empty(KDTree::KDNode*& head) {
    if(head != nullptr) {
        empty(head->left);
        empty(head->right);
        delete head;
    }
    head = nullptr;
}

KDTree::KDTree(std::vector<std::vector<int>> points, int depth = 0) {
    this -> root = KDTree::buildKDTree(points, depth);
}

typename KDTree::KDNode* KDTree::buildKDTree(std::vector<std::vector<int>> points, int depth = 0) {
    if(points.size() == 0)
        return nullptr;
    else {
        mergeSort(points, 0, points.size() - 1, depth % points[0].size());
        int median = floor(points.size() / 2);
        auto newNode = new KDNode(points[median], depth % points[0].size());
        std::vector<std::vector<int>> leftBranch(points.begin(), points.begin() + median);
        std::vector<std::vector<int>> rightBranch(points.begin() + median + 1, points.end());
        newNode->left = buildKDTree(leftBranch, depth + 1);
        newNode->right = buildKDTree(rightBranch, depth + 1);
        return newNode;
    }
}

typename KDTree::KDNode* KDTree::closest(KDTree::KDNode* node1, KDTree::KDNode* node2, std::vector<int> point) {
    if(node1 == nullptr)
        return node2;
    if(node2 == nullptr)
        return node1;
    
    double distanceNode1 = distanceSquared(node1->point, point);
    double distanceNode2 = distanceSquared(node2->point, point);

    if(distanceNode1 < distanceNode2)
        return node1;
    else
        return node2;
}


typename KDTree::KDNode* KDTree::closestPoint(KDTree::KDNode* head, std::vector<int> point, int depth) {
    if(head == nullptr)
        return nullptr;
    
    KDNode *nextBranch, *otherBranch;

    if(point[head->axis] < head->point[head->axis]) {
        nextBranch = head->left;
        otherBranch = head->right;
    } else {
        nextBranch = head->right;
        otherBranch = head->left;
    }
    
    auto temp = closestPoint(nextBranch, point, depth + 1);
    auto best = closest(head, temp, point);
    
    double distanceBest = distanceSquared(point, best->point);
    int distanceAxis = std::abs(point[head->axis] - head->point[head->axis]);

    if(distanceAxis <= distanceBest) {
        temp = closestPoint(otherBranch, point, depth + 1);
        best = closest(best, temp, point);
    }
    return best;
}

BPQ KDTree::KNNClassifier(std::vector<int> point, int k) {
    BPQ nearestPoints(k);
    this->KNNClassifier(nearestPoints, this->root, point);
    return nearestPoints;
}

void KDTree::KNNClassifier(BPQ &nearestPoints, KDTree::KDNode* head, std::vector<int> point) {
    if(head == nullptr)
        return;
    
    nearestPoints.insert(head->point, distanceSquared(head->point, point));
    KDNode *nextBranch, *otherBranch;

    if(point[head->axis] < head->point[head->axis]) {
        nextBranch = head->left;
        otherBranch = head->right;
    } else {
        nextBranch = head->right;
        otherBranch = head->left;
    }

    this->KNNClassifier(nearestPoints, nextBranch, point);
    int distanceAxis = std::abs(point[head->axis] - head->point[head->axis]);
    double distanceTemp = nearestPoints.queue[nearestPoints.queue.size() - 1].second;

    if(nearestPoints.queue.size() < nearestPoints.size || distanceAxis < distanceTemp)
        this->KNNClassifier(nearestPoints, otherBranch, point);
}

void KDTree::closestPoint(std::vector<int> point) {
    auto nearestPoint = closestPoint(this->root, point, 0);
    for(int i = 0; i < point.size(); ++i)
        std::cout << nearestPoint->point[i] << " ";
    std::cout << std::endl;
}

void KDTree::printKDTree() {
    if(this->root == nullptr)
        std::cout << "The tree is empty..." << std::endl;
    else
        printKDTree(this->root);
}

void KDTree::printKDTree(KDTree::KDNode *head) {
    if(head != nullptr) {
        printKDTree(head->left);
        for(int i = 0; i < head->point.size(); ++i)
            std::cout << head->point[i] << " ";
        std::cout << std::endl;
        printKDTree(head->right);
    }
}

int KDTree::getHeight() {
    return getHeight(this->root);
}

int KDTree::getHeight(KDTree::KDNode* head) {
    if(head == nullptr)
        return 0;
    return std::max(getHeight(head->left), getHeight(head->right)) + 1;
}
