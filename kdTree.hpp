#ifndef KDTREE_H
#define KDTREE_H

#include "libraries.hpp"
#include "BPQ.hpp"

class KDTree {
    public:
        KDTree();
        KDTree(std::vector<std::vector<int>> points, int depth);
        ~KDTree();

        void printKDTree();
        void closestPoint(std::vector<int> point);
        BPQ KNNClassifier(std::vector<int> point, int k);

        int getHeight();

    private:
        struct KDNode {
            std::vector<int> point;
            KDNode* left;
            KDNode* right;
            int axis;

            KDNode(std::vector<int> point, int axis) {
                this -> point = point;
                this -> axis = axis;
                left = nullptr;
                right = nullptr;
            }
        };

        KDNode* root;

        KDNode* buildKDTree(std::vector<std::vector<int>> points, int depth);
        KDNode* closestPoint(KDNode* head, std::vector<int> point, int depth);
        KDNode* closest(KDNode* node1, KDNode* node2, std::vector<int> point);
        void KNNClassifier(BPQ &nearestPoint, KDNode* head, std::vector<int> point);

        void printKDTree(KDNode* head);
        void empty(KDNode*& head);
        int getHeight(KDNode* head);
};


#endif